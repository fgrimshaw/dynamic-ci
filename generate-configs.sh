#!/bin/bash

cat > config-a.yml <<HEREDOC
service-a-1:
  script:
    - echo 'job 1 for service A'
service-a-2:
  script:
    - echo 'job 2 for service A'
HEREDOC

cat > config-b.yml <<HEREDOC
service-b-1:
  script:
    - echo 'job 1 for service B'
service-b-2:
  script:
    - echo 'job 2 for service B'
HEREDOC